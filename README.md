# Simplified-LC-3-FPGA-implementation
(implementation of Lab 6 for ECE 385)
### By Aaron Chen, Shruti Chanumolu

Implementation of subset of LC-3 instructions (SLC-3) in SystemVerilog on FPGA.

The SLC-3 is a simple 16-bit microprocessor that processes a subset of the LC3 instruction set, and is implemented using SystemVerilog. It is designed to be programmed onto the FPGA on the DE2-115 board and has special addresses to which the LC3 can write to such that it will display hex values or turn on LEDs, while also displaying the PC value on the other hex displays.

slc3.sv should be set as the top-level module

### Block Diagram
<img src="/block_diagram.png" alt="block_diagram" />

### Carry-Select Adder
<img src="/state_machine.png" alt="state_machine" />
