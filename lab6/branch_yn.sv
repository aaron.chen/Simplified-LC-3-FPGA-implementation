module branch_yn
(
	input logic [2:0] nzp_ir, nzp_curr,
	output logic out
);

always_comb
begin
	if (nzp_ir[0] == 1'b1 && nzp_curr[0] == 1'b1)
		out = 1'b1;
	else if (nzp_ir[1] == 1'b1 && nzp_curr[1] == 1'b1)
		out = 1'b1;
	else if (nzp_ir[2] == 1'b1 && nzp_curr[2] == 1'b1)
		out = 1'b1;
	else
		out = 1'b0;
end

endmodule : branch_yn