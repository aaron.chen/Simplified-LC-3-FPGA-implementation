module adder #(parameter width = 16)
(
	input logic [width-1:0] a1, a0,
	output logic [width-1:0] sum
);

assign sum = a1 + a0;
	
endmodule : adder