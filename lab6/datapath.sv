module datapath
(
	//CLOCK
	input logic Clk,

	//DATAPATH <--- CONTROL
	//reg signals
	input load_pc,
	input load_ir,
	input load_mar,
	input load_mdr,
	input load_ben,
	input load_nzp,
	input load_regfile,
	input Reset,

	//mux signals
	input  gate_marmux_sel,
	input  gate_pc_sel,
	input  gate_mdr_sel,
	input  gate_alu_sel,
	input  [1:0] pc_mux_sel,
	input  dr_mux_sel,
	input  sr2_mux_sel,
	input  sr1_mux_sel,
	input  [1:0] addr2_mux_sel,
	input  addr1_mux_sel,
	input  mio_mux_sel,

	//alu
	input  [1:0] alu_op,

	//DATAPATH ---> CONTROL
	//reg outs
	output logic [15:0] ir_out,
	output logic ben_out,
	output logic [15:0] pc_out,

	//DATAPATH ---> MEMORY
	output logic [15:0] mar_out,
	output logic [15:0] mdr_out,

	//DATAPATH <--- MEMORY
	input logic [15:0] data_to_cpu
);

//internal wires for anything not already input or output
//regs

logic [2:0] nzp_out;
logic [15:0] regfile_sr2_out;
logic [15:0] regfile_sr1_out;
//muxes
logic [15:0] gate_mux_out;
logic [15:0] pc_mux_out;
logic [15:0] sr2_mux_out;
logic [2:0] sr1_mux_out;
logic [2:0] dr_mux_out;
logic [15:0] addr2_mux_out;
logic [15:0] addr1_mux_out;
logic [15:0] mio_mux_out;
//sext offsets
logic [15:0] ir_off11_out;
logic [15:0] ir_off9_out;
logic [15:0] ir_off6_out;
logic [15:0] ir_imm5_out;
//misc
logic [15:0] addr_plus_offset_out;
logic [15:0] pc_plus1_out;
logic [2:0] genNZP_out;
logic branch_yn_out;
logic [15:0] alu_out;

logic [1:0] gate_mux_sel;

always_comb
begin
	if (gate_marmux_sel == 1'b1)
		gate_mux_sel = 2'b11;
	else if (gate_pc_sel == 1'b1)
		gate_mux_sel = 2'b10;
	else if (gate_mdr_sel == 1'b1)
		gate_mux_sel = 2'b01;
	else
		gate_mux_sel = 2'b00;
end

register pc_reg
(
	.Clk,
	.Reset,
	.load_enable(load_pc),
	.data_in(pc_mux_out),
	.data_out(pc_out)
);

register ir_reg
(
	.Clk,
	.Reset,
	.load_enable(load_ir),
	.data_in(gate_mux_out),
	.data_out(ir_out)
);

register mar_reg
(
	.Clk,
	.Reset,
	.load_enable(load_mar),
	.data_in(gate_mux_out),
	.data_out(mar_out)
);

register mdr_reg
(
	.Clk,
	.Reset,
	.load_enable(load_mdr),
	.data_in(mio_mux_out),
	.data_out(mdr_out)
);

register #(.width(1)) ben_reg
(
	.Clk,
	.Reset,
	.load_enable(load_ben),
	.data_in(branch_yn_out),
	.data_out(ben_out)
);

register #(.width(3)) nzp_reg
(
	.Clk,
	.Reset,
	.load_enable(load_nzp),
	.data_in(genNZP_out),
	.data_out(nzp_out)
);

regfile regfile_unit
(
	.Clk(Clk),
	.Reset(Reset),
	.load_dr_enable(load_regfile),
	.data_in(gate_mux_out),
	.sr2_in(ir_out[2:0]), .sr1_in(sr1_mux_out), .dr_in(dr_mux_out),
	.sr2_out(regfile_sr2_out), .sr1_out(regfile_sr1_out)
);

mux4to1 gate_mux 
(
	.sel(gate_mux_sel),
	.a3(addr_plus_offset_out),
	.a2(pc_out),
	.a1(mdr_out),
	.a0(alu_out),
	.out(gate_mux_out)
);

mux3to1 pc_mux 
(
	.sel(pc_mux_sel),
	.a2(gate_mux_out),
	.a1(addr_plus_offset_out),
	.a0(pc_plus1_out),
	.out(pc_mux_out)
);

mux2to1 #(.width(3)) dr_mux 
(
	.sel(dr_mux_sel),
	.a1(3'b111),
	.a0(ir_out[11:9]),
	.out(dr_mux_out)
);

mux2to1 sr2_mux
(
	.sel(sr2_mux_sel),
	.a1(ir_imm5_out),
	.a0(regfile_sr2_out),
	.out(sr2_mux_out)
);

mux2to1 #(.width(3)) sr1_mux
(
	.sel(sr1_mux_sel),
	.a1(ir_out[11:9]),
	.a0(ir_out[8:6]),
	.out(sr1_mux_out)
);

mux4to1 addr2_mux 
(
	.sel(addr2_mux_sel),
	.a3(ir_off11_out),
	.a2(ir_off9_out),
	.a1(ir_off6_out),
	.a0(16'h0000),
	.out(addr2_mux_out)
);

mux2to1 addr1_mux
(
	.sel(addr1_mux_sel),
	.a1(regfile_sr1_out),
	.a0(pc_out),
	.out(addr1_mux_out)
);

mux2to1 mio_mux 
(
	.sel(mio_mux_sel),
	.a1(data_to_cpu),
	.a0(gate_mux_out),
	.out(mio_mux_out)
);

sext #(.in_width(11), .out_width(16)) ir_off11
(
	.data_in(ir_out[10:0]),
	.data_out(ir_off11_out)
);

sext #(.in_width(9), .out_width(16)) ir_off9
(
	.data_in(ir_out[8:0]),
	.data_out(ir_off9_out)
);

sext #(.in_width(6), .out_width(16)) ir_off6
(
	.data_in(ir_out[5:0]),
	.data_out(ir_off6_out)
);

sext #(.in_width(5), .out_width(16)) ir_imm5
(
	.data_in(ir_out[4:0]),
	.data_out(ir_imm5_out)
);

adder addr_plus_offset
(
	.a1(addr2_mux_out),
	.a0(addr1_mux_out),
	.sum(addr_plus_offset_out)
);

plus_const #(.width(16), .step(1'b1)) pc_plus1
(
	.data_in(pc_out),
	.data_out(pc_plus1_out)
);

genNZP genNZP_unit
(
	.data_in(gate_mux_out),
	.nzp_code(genNZP_out)
);

branch_yn branch_yn_unit
(
	.nzp_ir(ir_out[11:9]),
	.nzp_curr(nzp_out),
	.out(branch_yn_out)
);

alu alu_unit
(
	.op(alu_op),
	.b(sr2_mux_out),
	.a(regfile_sr1_out),
	.data_out(alu_out)
);

endmodule : datapath