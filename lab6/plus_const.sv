module plus_const #(parameter width = 16, step = 1'b1)
(
	input logic [width-1:0] data_in,
	output logic [width-1:0] data_out
);

assign data_out = data_in + step;

endmodule : plus_const