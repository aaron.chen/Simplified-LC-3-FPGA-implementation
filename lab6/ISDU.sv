//------------------------------------------------------------------------------
// Company:          UIUC ECE Dept.
// Engineer:         Stephen Kempf
//
// Create Date:    17:44:03 10/08/06
// Design Name:    ECE 385 Lab 6 Given Code - Incomplete ISDU
// Module Name:    ISDU - Behavioral
//
// Comments:
//    Revised 03-22-2007
//    Spring 2007 Distribution
//    Revised 07-26-2013
//    Spring 2015 Distribution
//    Revised 02-13-2017
//    Spring 2017 Distribution
//------------------------------------------------------------------------------


module ISDU (   input logic         Clk, 
                                    Reset,
                                    Run,
                                    Continue,
                                    
                input logic[3:0]    Opcode, 
                input logic         IR_5,
                input logic         IR_11,
                input logic         BEN,
                  
                output logic        LD_MAR,
                                    LD_MDR,
                                    LD_IR,
                                    LD_BEN,
                                    LD_CC,
                                    LD_REG,
                                    LD_PC,
                                    LD_LED, // for PAUSE instruction
                                    
                output logic        GatePC,
                                    GateMDR,
                                    GateALU,
                                    GateMARMUX,
                                    
                output logic [1:0]  PCMUX,
                output logic        DRMUX,
                                    SR1MUX,
                                    SR2MUX,
                                    ADDR1MUX,
                output logic [1:0]  ADDR2MUX,
                                    ALUK,
                  
                output logic        Mem_CE,
                                    Mem_UB,
                                    Mem_LB,
                                    Mem_OE,
                                    Mem_WE
                );

    enum logic [4:0] {  Halted, 
                        PauseIR1, 
                        PauseIR2, 
                        S_18, 
                        S_33_1, 
                        S_33_2, 
                        S_35, 
								S_32, 
								S_1,
								S_5,
								S_9,
								S_6,
								S_25_1,
								S_25_2,
								S_27,
								S_7,
								S_23,
								S_16_1,
								S_16_2,
								S_16_3,
								S_4,
								S_21,
								S_12,
								S_0,
								S_22}   State, Next_state;   // Internal state logic
        
    always_ff @ (posedge Clk)
    begin
        if (Reset) 
            State <= Halted;
        else 
            State <= Next_state;
    end
   
    always_comb
    begin 
        // Default next state is staying at current state
        Next_state = State;
     
        unique case (State)
            Halted : 
                if (Run) 
                    Next_state = S_18;                      
            S_18 : 
                Next_state = S_33_1;
            // Any states involving SRAM require more than one clock cycles.
            // The exact number will be discussed in lecture.
            S_33_1 : 
                Next_state = S_33_2;
            S_33_2 : 
                Next_state = S_35;
            S_35 : 
                Next_state = S_32;
            // PauseIR1 and PauseIR2 are only for Week 1 such that TAs can see 
            // the values in IR.
        
				S_32 :// You need to finish the rest of opcodes..... 
               unique case (Opcode)
						4'b0000 : //BR
							Next_state = S_0;
						4'b0001 : //ADD
							Next_state = S_1;
						4'b0101 : //AND
							Next_state = S_5;
						4'b0100 : //JSR
							Next_state = S_4;
						4'b0110 : //LDR
							Next_state = S_6;
						4'b0111 : //STR
							Next_state = S_7;
						4'b1001 : //NOT
							Next_state = S_9;
						4'b1100 : //JMP
							Next_state = S_12;
						4'b1101 : //PAUSE
							Next_state = PauseIR1;
                  default : 
                     Next_state = S_18;
                endcase
            
				PauseIR1 : 
                if (~Continue) 
                    Next_state = PauseIR1;
                else 
                    Next_state = PauseIR2;
            PauseIR2 : 
                if (Continue) 
                    Next_state = PauseIR2;
                else 
                    Next_state = S_18;
						  
				S_1 : //ADD
					 Next_state = S_18;
				
				S_5 : //AND
					 Next_state = S_18;
				
				S_9 : //NOT
					 Next_state = S_18;
				
				S_0 : //BR0
					if(BEN)
						Next_state = S_22;
					else 
						Next_state = S_18;
				S_22 : //BR1
					 Next_state = S_18;
				
				S_12 : //JMP
					 Next_state = S_18;
					 
				S_4 : //JSR0
					 Next_state = S_21;
				S_21 : //JSR1
					 Next_state = S_18;
				
				S_6 : //LDR0
					 Next_state = S_25_1;
				S_25_1 : //LDR1
					 Next_state = S_25_2;
				S_25_2 : //LDR2
					 Next_state = S_27;
				S_27 : //LDR3
					 Next_state	= S_18;
				
				S_7 : //STR0
					 Next_state = S_23;
				S_23 : //STR1
					 Next_state = S_16_1;
				S_16_1 : //STR2
					 Next_state = S_16_2;
				S_16_2 : //STR3
					Next_state = S_16_3;
				S_16_3 :
					Next_state = S_18;
            
				default : ;

        endcase

        // default controls signal values; within a process, these can be
        // overridden further down (in the case statement, in this case)
        LD_MAR = 1'b0;
        LD_MDR = 1'b0;
        LD_IR = 1'b0;
        LD_BEN = 1'b0;
        LD_CC = 1'b0;
        LD_REG = 1'b0;
        LD_PC = 1'b0;
        LD_LED = 1'b0;
         
        GatePC = 1'b0;
        GateMDR = 1'b0;
        GateALU = 1'b0;
        GateMARMUX = 1'b0;
         
        ALUK = 2'b00;
         
        PCMUX = 2'b00;
        DRMUX = 1'b0;
        SR1MUX = 1'b0;
        SR2MUX = 1'b0;
        ADDR1MUX = 1'b0;
        ADDR2MUX = 2'b00;
         
        Mem_OE = 1'b1;
        Mem_WE = 1'b1;
        
        // Assign control signals based on current state
        case (State)
            Halted: ;
            S_18 : 
                begin 
                    GatePC = 1'b1;
                    LD_MAR = 1'b1;
                    PCMUX = 2'b00;
                    LD_PC = 1'b1;
                end
            
				S_33_1 : 
					begin
						Mem_OE = 1'b0;
						LD_MDR = 1'b1;
					end
            
				S_33_2 : //Load MDR
                begin 
                    Mem_OE = 1'b0;
                    LD_MDR = 1'b1;
                end
            
				S_35 : //Load IR
                begin 
                    GateMDR = 1'b1;
                    LD_IR = 1'b1;
                end
            
				PauseIR1: 
					LD_LED = 1'b1;
            PauseIR2:
					LD_LED = 1'b1;
            
				S_32 : //load BEN
                LD_BEN = 1'b1;
            
				S_1 : //ADD
					 begin 
						  SR2MUX = IR_5;
						  ALUK = 2'b00;
						  GateALU = 1'b1;
						  LD_REG = 1'b1;
						  LD_CC = 1'b1;
						  // complete...
					 end
					
				S_5 : //AND
					begin
							SR2MUX = IR_5;
							ALUK = 2'b01;
							GateALU = 1'b1;
							LD_REG = 1'b1;
							LD_CC = 1'b1;
					end
					
				S_9 : //NOT
					begin
							ALUK = 2'b10;
							GateALU = 1'b1;
							LD_REG = 1'b1;
							LD_CC = 1'b1;
					end
				
				S_0 : //BR0 //doubtful
					begin
					end
				
				S_22 : //BR1
					begin
							ADDR1MUX = 1'b0;
							ADDR2MUX = 2'b10;
							PCMUX = 2'b01;
							LD_PC = 1'b1;
					end
					
				S_12 : //JMP
					begin
							ALUK = 2'b11;
							GateALU = 1'b1;
							PCMUX = 2'b10;
							LD_PC = 1'b1;
					end
				
				S_4 : //JSR0
					begin
							GatePC = 1'b1;
							DRMUX = 1'b1;
							LD_REG = 1'b1;
					end
				
				S_21 : //JSR1
					begin
							ADDR1MUX = 1'b0;
							ADDR2MUX = 2'b11;
							PCMUX = 2'b01;
							LD_PC = 1'b1;
					end
			
				S_6 : //LDR0 
					begin
							ADDR1MUX = 1'b1;
							ADDR2MUX = 2'b01;
							GateMARMUX = 1'b1;
							LD_MAR = 1'b1;
					end
				
				S_25_1 : //LDR1
					begin
							Mem_OE = 1'b0;
							LD_MDR = 1'b1;
					end
				
				S_25_2 : //LDR2
					begin
							Mem_OE = 1'b0;
							LD_MDR = 1'b1;
					end
				
				S_27 : //LDR3
					begin
							GateMDR = 1'b1;
							LD_REG = 1'b1;
							LD_CC = 1'b1;
					end
				
				S_7 : //STR0
					begin
							ADDR1MUX = 1'b1;
							ADDR2MUX = 2'b01;
							GateMARMUX = 1'b1;
							LD_MAR = 1'b1;
					end
				
				S_23 : //STR1
					begin
							SR1MUX = 1'b1;
							ALUK = 2'b11;
							GateALU = 1'b1;
							LD_MDR = 1'b1;
					end
				
				S_16_1 : //STR2
					Mem_WE = 1'b0;
				
				S_16_2 : //STR3
					Mem_WE = 1'b0;
				
				S_16_3 :
					Mem_WE = 1'b0;
				
				default : ;
        endcase
    end 

     // These should always be active
    assign Mem_CE = 1'b0;
    assign Mem_UB = 1'b0;
    assign Mem_LB = 1'b0;
    
endmodule
