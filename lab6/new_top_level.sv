module new_top_level
(
	input logic [15:0] S,
	input logic Clk, Reset, Run, Continue,
	inout wire [15:0] Data, //tristate buffers need to be of type wire
	
	output logic [11:0] LED,
	output logic [6:0] HEX0, HEX1, HEX2, HEX3, HEX4, HEX5, HEX6, HEX7,
	output logic CE, UB, LB, OE, WE,
	output logic [19:0] ADDR
);

slc3 real_top_level(.*);

test_memory testmem(
	.Clk,
	.Reset, 
	.I_O(Data),
	.A(ADDR),
	.CE,
	.UB,
	.LB,
	.OE,
	.WE
);

endmodule : new_top_level