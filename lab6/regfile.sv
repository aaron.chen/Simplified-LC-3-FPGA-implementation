module regfile #(parameter width = 16, len = 8)
(
	input logic Clk, Reset,
	input logic load_dr_enable,
	input logic [width-1:0] data_in,
	input logic [2:0] sr2_in, sr1_in, dr_in,
	output logic [width-1:0] sr2_out, sr1_out
);
//
//logic [len-1:0] load_enable_arr; //load enable signals
//logic [len-1:0] [width-1:0] data_out_arr; //register out signals
//
//generate
//	genvar i;
//	for (i = 0; i < len; i++) begin : register_gen
//		register register_arr
//		(
//			.Clk(Clk),
//			.load_enable(load_enable_arr[i]),
//			.data_in(data_in),
//			.data_out(data_out_arr[i])
//		);
//	end
//endgenerate
//
//initial
//begin
//	for (int i = 0; i < len; i++)
//	begin
//		load_enable_arr[i] = 1'b0;
//		data_out_arr[i] = {width{1'b0}};
//	end
//end
//
//always_comb
//begin
//	if (load_dr_enable == 1)
//		load_enable_arr[dr_in] = 1'b1;
//end

logic [width-1:0] data_out_arr[len]; //register out signals

always_ff @ (posedge Clk)
begin
	if(Reset)begin
		for(int i = 0;i < len; i++)
		begin
			data_out_arr[i] = {width{1'b0}};
		end
	end	
	else begin
		if(load_dr_enable)
			data_out_arr[dr_in] = data_in;
	end
			
end

assign sr2_out = data_out_arr[sr2_in];
assign sr1_out = data_out_arr[sr1_in];

endmodule : regfile
