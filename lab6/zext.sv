// out = ZEXT[in] to out_width bits
module zext #(parameter in_width = 16, out_width = 20)
(
	input logic [in_width-1:0] data_in,
	output logic [out_width-1:0] data_out
);

assign data_out = $unsigned(data_in);

endmodule : zext